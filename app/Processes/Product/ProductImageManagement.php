<?php

namespace App\Processes\Product;

use App\Models\Product;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Constraint;
use Intervention\Image\Image;

class ProductImageManagement
{
    /**
     * @var Image
     */
    private $image;

    /**
     * @var string|null
     */
    private $destination;

    /**
     * @var string
     */
    private $file_name;


    public function __construct(Product $product, Image $image, ?string $destination = null)
    {
        $this->image       = $image;
        $this->destination = $destination ?? $this->destination();
        $this->file_name   = ($product->id . '.png');
    }

    /**
     * @return Image
     */
    public function save(): Image
    {
        if (!is_dir($this->destination) && !mkdir($directory = $this->destination, 0755, true) && !is_dir($directory)) {
            throw new \RuntimeException(sprintf('Directory "%s" was not created', $directory));
        }

        return $this->resize($this->image)->encode('png')->save($this->destination . $this->file_name);
    }

    /**
     * @param Image $image
     * @param int $width
     * @param int $height
     *
     * @return Image
     */
    private function resize(Image $image, int $width = 250, int $height = 250): Image
    {
        return $image->resize($width, $height, static function (Constraint $constraint) {
            $constraint->aspectRatio();
        });
    }

    /**
     * @return string
     */
    private function destination(): string
    {
        return Storage::disk('public')->getAdapter()->getPathPrefix() . DIRECTORY_SEPARATOR . 'products' . DIRECTORY_SEPARATOR;
    }
}