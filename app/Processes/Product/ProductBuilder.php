<?php

namespace App\Processes\Product;

use App\Models\Product;

class ProductBuilder
{
    /**
     * @var ProductBuilderDataObject
     */
    private $builder_data_object;

    public function __construct(ProductBuilderDataObject $builder_data_object)
    {
        $this->builder_data_object = $builder_data_object;
    }

    /**
     * @param Product|null $product
     *
     * @return Product
     */
    public function build(?Product $product = null): Product
    {
        $creation = $product === null;

        if ($creation) {
            $product = new Product();
        }

        $product->name        = $this->builder_data_object->getName();
        $product->price       = $this->builder_data_object->getPrice();
        $product->deposit     = $this->builder_data_object->getDeposit();
        $product->description = $this->builder_data_object->getDescription();

        $product->save();

        return $product;
    }
}