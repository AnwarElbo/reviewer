<?php

namespace App\Processes\Shop;

use App\Models\City;
use App\Models\Shop;
use App\Models\ShopContact;
use App\Models\ShopOpeningHour;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;

class ShopBuilder
{
    /**
     * @var ShopBuilderDataObject
     */
    private $shop_creator_data_object;

    public function __construct(ShopBuilderDataObject $shop_creator_data_object)
    {
        $this->shop_creator_data_object = $shop_creator_data_object;
    }

    /**
     * @param Shop|null $shop
     *
     * @return Shop
     */
    public function build(?Shop $shop = null): Shop
    {
        $creation = $shop === null;
        DB::beginTransaction();
        try {

        if ($creation) {
            $shop = new Shop();
        }

        $shop->name        = $this->shop_creator_data_object->getName();
        $shop->title       = $this->shop_creator_data_object->getTitle();
        $shop->description = $this->shop_creator_data_object->getDescription();
        $shop->slug        = $this->shop_creator_data_object->getSlug();
        $shop->viewed      = 0;

        $shop->save();

        $this->buildShopContact($shop, $creation ? null : $shop->contact);
        $this->buildOpeningHours($shop);
        $this->syncProducts($shop);

        } catch (QueryException $exception) {
            DB::rollBack();

            throw $exception;
        }

        DB::commit();

        return $shop;
    }

    /**
     * @param Shop $shop
     * @param ShopContact|null $shop_contact
     *
     * @return ShopContact
     */
    private function buildShopContact(Shop $shop, ?ShopContact $shop_contact = null): ShopContact
    {
        if ($shop_contact === null) {
            $shop_contact = new ShopContact();
        }

        $cities = $this->shop_creator_data_object->getCities()->mapWithKeys(function (array $city) {
            return [$city['id'] => ['primary' => $this->shop_creator_data_object->getPrimaryCity() === $city['id']]];
        })->toArray();


        $shop_contact->phone_number = $this->shop_creator_data_object->getPhoneNumber();
        $shop_contact->street       = $this->shop_creator_data_object->getStreet();
        $shop_contact->house_number = $this->shop_creator_data_object->getHouseNumber();
        $shop_contact->postal_code  = $this->shop_creator_data_object->getPostalCode();
        $shop_contact->shop()->associate($shop);
        $shop_contact->save();

        $shop_contact->cities()->sync($cities);

        return $shop_contact;
    }

    /**
     * @param Shop $shop
     */
    private function buildOpeningHours(Shop $shop): void
    {
        $shop->openingHours()->delete();

        $this->shop_creator_data_object->getOpeningHours()->each(static function (array $opening_hours, int $day) use ($shop) {
            $opening_hour = new ShopOpeningHour();

            $opening_hour->day       = $day;
            $opening_hour->open_from = new \Datetime($opening_hours['open_from']);
            $opening_hour->open_till = new \DateTime($opening_hours['open_till']);
            $opening_hour->shop()->associate($shop);

            $opening_hour->save();
        });
    }

    /**
     * @param Shop $shop
     */
    private function syncProducts(Shop $shop): void
    {
        $shop->products()->sync($this->shop_creator_data_object->getProducts());
    }
}