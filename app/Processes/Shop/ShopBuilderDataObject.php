<?php

namespace App\Processes\Shop;

use Illuminate\Support\Collection;

class ShopBuilderDataObject
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $slug;

    /**
     * @var string
     */
    private $phone_number;

    /**
     * @var string
     */
    private $street;


    /**
     * @var string
     */
    private $house_number;

    /**
     * @var Collection
     */
    private $cities;

    /**
     * @var string
     */
    private $postal_code;

    /**
     * @var Collection
     */
    private $opening_hours;

    /**
     * @var array
     */
    private $products;

    /**
     * @var int
     */
    private $primary_city;

    public function __construct(
        string $name,
        string $title,
        string $description,
        string $slug,
        string $phone_number,
        string $street,
        string $house_number,
        int   $primary_city,
        array $cities,
        string $postal_code,
        array $opening_hours,
        array $products
    ) {
        $this->name          = $name;
        $this->title         = $title;
        $this->description   = $description;
        $this->slug          = $slug;
        $this->phone_number  = $phone_number;
        $this->street        = $street;
        $this->house_number  = $house_number;
        $this->cities        = collect($cities);
        $this->postal_code   = $postal_code;
        $this->opening_hours = collect($opening_hours);
        $this->products      = $products;
        $this->primary_city = $primary_city;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return strtolower($this->slug);
    }

    /**
     * @return string
     */
    public function getPhoneNumber(): string
    {
        return $this->phone_number;
    }

    /**
     * @return string
     */
    public function getStreet(): string
    {
        return $this->street;
    }

    /**
     * @return string
     */
    public function getHouseNumber(): string
    {
        return $this->house_number;
    }

    /**
     * @return Collection
     */
    public function getCities(): Collection
    {
        return $this->cities;
    }

    /**
     * @return string
     */
    public function getPostalCode(): string
    {
        return $this->postal_code;
    }

    /**
     * @return Collection
     */
    public function getOpeningHours(): Collection
    {
        return $this->opening_hours;
    }

    /**
     * @return array
     */
    public function getProducts(): array
    {
        return $this->products;
    }

    /**
     * @return int
     */
    public function getPrimaryCity(): int
    {
        return $this->primary_city;
    }
}