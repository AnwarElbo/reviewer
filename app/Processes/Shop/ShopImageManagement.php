<?php

namespace App\Processes\Shop;

use App\Models\Shop;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Constraint;
use Intervention\Image\Image;

class ShopImageManagement
{
    /**
     * @var Shop
     */
    private $shop;

    /**
     * @var Image
     */
    private $image;

    /**
     * @var string|null
     */
    private $destination;

    /**
     * @var string
     */
    private $file_name;


    public function __construct(Shop $shop, Image $image, ?string $destination = null)
    {
        $this->shop        = $shop;
        $this->image       = $image;
        $this->destination = $destination ?? $this->destination();
        $this->file_name   = (Str::random(5) . '_' . time() . '.png');
    }

    /**
     * @return Image
     */
    public function save(): Image
    {
        if (!is_dir($this->destination) && !mkdir($directory = $this->destination, 0755, true) && !is_dir($directory)) {
            throw new \RuntimeException(sprintf('Directory "%s" was not created', $directory));
        }

        return $this->resize($this->image)->encode('png')->save($this->destination . $this->file_name);
    }

    /**
     * @param Image $image
     * @param int $width
     * @param int $height
     *
     * @return Image
     */
    private function resize(Image $image, int $width = 250, int $height = 250): Image
    {
        return $image->resize($width, $height, static function (Constraint $constraint) {
            $constraint->aspectRatio();
        });
    }

    /**
     * @return string
     */
    private function destination(): string
    {
        return Storage::disk('public')->getAdapter()->getPathPrefix() . DIRECTORY_SEPARATOR . $this->shop->id . DIRECTORY_SEPARATOR;
    }
}