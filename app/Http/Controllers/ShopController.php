<?php

namespace App\Http\Controllers;

use App\Http\Resources\ShopResource;
use App\Models\Shop;
use App\Processes\Shop\ShopBuilder;
use App\Processes\Shop\ShopBuilderDataObject;
use App\Processes\Shop\ShopImageManagement;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Intervention\Image\Facades\Image;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ShopController extends Controller
{
    /**
     * @return AnonymousResourceCollection
     */
    public function popular(): AnonymousResourceCollection
    {
        return ShopResource::collection(
            Shop::orderByDesc('viewed')
                ->with(['products', 'contact', 'reviews'])
                ->get()
                ->filter(static function (Shop $shop) {
                    return $shop->isOpen();
                })
        );
    }

    /**
     * @param string $city
     *
     * @return AnonymousResourceCollection
     */
    public function city(string $city): AnonymousResourceCollection
    {
        return ShopResource::collection(Shop::whereHas('contact.cities',
            static function (Builder $builder) use ($city) {
                $builder->where('name', ucfirst(strtolower($city)));
            })->with(['contact', 'reviews', 'products'])
            ->orderByDesc('viewed')
            ->limit(9)
            ->get());
    }

    /**
     * @param string $province
     *
     * @return AnonymousResourceCollection
     */
    public function province(string $province): AnonymousResourceCollection
    {
        return ShopResource::collection(Shop::whereHas('contact.cities.province',
            static function (Builder $builder) use ($province) {
                $builder->where('name', ucfirst(strtolower($province)));
            })->with(['contact.cities', 'products', 'reviews', 'products', 'openingHours'])
            ->orderByDesc('viewed')
            ->limit(9)
            ->get());
    }

    /**
     * @return AnonymousResourceCollection
     */
    public function index(): AnonymousResourceCollection
    {
        return ShopResource::collection(Shop::with(['contact', 'reviews', 'products'])->get());
    }

    /**
     * @param Request $request
     *
     * @return ShopResource
     */
    public function create(Request $request): ShopResource
    {
        $data_object = $this->createShopBuilderDataObject($request);

        return new ShopResource((new ShopBuilder($data_object))->build());
    }

    /**
     * @param Request $request
     * @param Shop    $shop
     *
     * @return JsonResponse
     */
    public function uploadImage(Request $request, Shop $shop): JsonResponse
    {
        $validated_data = $request->validate([
            'images' => 'required|array'
        ]);

        foreach ($validated_data['images'] as $image) {
            if (! $image instanceof UploadedFile) {
                return response()->json(['error' => 'Something went wrong'], 500);
            }

            $shop_image_management = new ShopImageManagement(
                $shop,
                Image::make($image)
            );

            $shop_image_management->save();
        }

        return response()->json(['message' => 'OK']);
    }

    /**
     * @param Shop $shop
     *
     * @return ShopResource
     */
    public function show(Shop $shop): ShopResource
    {
        $shop = $shop->load('contact.cities', 'products', 'reviews', 'products', 'openingHours');

        return new ShopResource($shop);
    }

    /**
     * @param Request $request
     * @param Shop    $shop
     *
     * @return ShopResource
     */
    public function update(Request $request, Shop $shop): ShopResource
    {
        $data_object = $this->createShopBuilderDataObject($request);

        return new ShopResource((new ShopBuilder($data_object))->build($shop));
    }

    /**
     * @param Shop $shop
     *
     * @return JsonResponse
     *
     * @throws \Exception
     */
    public function delete(Shop $shop): JsonResponse
    {
        $shop->delete();

        return response()->json([
            'message' => 'OK'
        ]);
    }

    /**
     * @param Request $request
     *
     * @return ShopBuilderDataObject
     */
    private function createShopBuilderDataObject(Request $request): ShopBuilderDataObject
    {
        $validated_data = $request->validate([
            'name'          => 'required|string',
            'title'         => 'required|string',
            'description'   => 'required|string',
            'slug'          => 'required|alpha_dash',
            'phone_number'  => 'required|string',
            'street'        => 'present|string',
            'house_number'  => 'present|string',
            'cities'        => 'required|array',
            'city'          => 'required|numeric',
            'postal_code'   => 'present|string',
            'opening_hours' => 'required|array',
            'products'      => 'required|array'
        ]);


        $data_object = new ShopBuilderDataObject(
            $validated_data['name'],
            $validated_data['title'],
            $validated_data['description'],
            $validated_data['slug'],
            $validated_data['phone_number'],
            $validated_data['street'],
            $validated_data['house_number'],
            $validated_data['city'],
            $validated_data['cities'],
            $validated_data['postal_code'],
            $validated_data['opening_hours'],
            $validated_data['products']
        );

        return $data_object;
    }
}
