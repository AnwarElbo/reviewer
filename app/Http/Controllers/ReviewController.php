<?php

namespace App\Http\Controllers;

use App\Http\Resources\ReviewResource;
use App\Models\Shop;
use App\Models\ShopReview;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ReviewController extends Controller
{
    /**
     * @param Request $request
     * @param Shop $shop
     *
     * @return ReviewResource
     */
    public function create(Request $request, Shop $shop): ReviewResource
    {
        $validated_data = $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'quality' => 'present|numeric|between:0,5',
            'service' => 'present|numeric|between:0,5',
            'price' => 'present|numeric|between:0,5',
            'comment' => 'present|string',
        ]);

        $review = new ShopReview();

        $review->name = $validated_data['name'];
        $review->email = $validated_data['email'];
        $review->quality = $validated_data['quality'];
        $review->service = $validated_data['service'];
        $review->price = $validated_data['price'];
        $review->comment = $validated_data['comment'];
        $review->ip_address = $request->ip();
        $review->shop()->associate($shop);

        $review->save();

        return new ReviewResource($review);
    }

    /**
     * @return AnonymousResourceCollection
     */
    public function index(): AnonymousResourceCollection
    {
        return ReviewResource::collection(ShopReview::withoutGlobalScopes()->orderByDesc('id')->get());
    }

    /**
     * @param int $review
     *
     * @return JsonResponse
     */
    public function toggleAllow(int $review): JsonResponse
    {
        $review        = ShopReview::withoutGlobalScopes()->find($review);
        $review->allow = ! $review->allow;
        $review->save();

        return response()->json(['message' => 'OK']);
    }

    public function increaseView(Shop $shop): JsonResponse
    {
        $shop->increaseViews();

        return response()->json(['message' => 'OK']);
    }
}
