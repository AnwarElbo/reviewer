<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

class SpaController extends Controller
{
    public function index()
    {
        if (Auth::check()) {
            return response()->redirectToRoute('auth');
        }

        return view('spa');
    }
}
