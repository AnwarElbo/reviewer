<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function contact(Request $request): JsonResponse
    {
        $validated_data = $request->validate([
            'name'    => 'required|string|max:255',
            'email'   => 'required|string|max:255',
            'message' => 'required|string',
        ]);

        $contact             = new Contact();
        $contact->name       = $validated_data['name'];
        $contact->email      = $validated_data['email'];
        $contact->message    = $validated_data['message'];
        $contact->ip_address = request()->ip();
        $contact->save();

        return response()->json(['message' => 'OK']);
    }
}
