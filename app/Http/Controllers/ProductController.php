<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProductResource;
use App\Models\Product;
use App\Processes\Product\ProductBuilder;
use App\Processes\Product\ProductBuilderDataObject;
use App\Processes\Product\ProductImageManagement;
use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Collection;
use Intervention\Image\Facades\Image;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ProductController extends Controller
{
    /**
     * @return AnonymousResourceCollection
     */
    public function index(): AnonymousResourceCollection
    {
        return ProductResource::collection(Product::with('shops')->get());
    }

    /**
     * @param Product $product
     *
     * @return ProductResource
     */
    public function show(Product $product): ProductResource
    {
        $product->load('shops');

        return new ProductResource($product);
    }

    /**
     * @param Product $product
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function delete(Product $product): JsonResponse
    {
        $product->delete();

        return response()->json(['message' => 'OK']);
    }

    /**
     * @param Request $request
     * @param Product $product
     *
     * @return JsonResponse
     */
    public function uploadImage(Request $request, Product $product): JsonResponse
    {
        $validated_data = $request->validate([
            'image' => 'required|file'
        ]);

        if (! ($image = $validated_data['image']) instanceof UploadedFile) {
            return response()->json(['error' => 'Something went wrong'], 500);
        }

        $shop_image_management = new ProductImageManagement(
            $product,
            Image::make($image)
        );

        $shop_image_management->save();

        return response()->json(['message' => 'OK']);
    }

    /**
     * @param Request $request
     * @param Product $product
     *
     * @return ProductResource
     */
    public function update(Request $request, Product $product): ProductResource
    {
        $data_object = $this->createProductBuilderDataObject($request);

        return new ProductResource((new ProductBuilder($data_object))->build($product));
    }

    /**
     * @param Request $request
     *
     * @return ProductResource
     */
    public function create(Request $request): ProductResource
    {
        $data_object = $this->createProductBuilderDataObject($request);

        return new ProductResource((new ProductBuilder($data_object))->build());
    }

    /**
     * @param Request $request
     *
     * @return ProductBuilderDataObject
     */
    private function createProductBuilderDataObject(Request $request): ProductBuilderDataObject
    {
        $validated_data = $request->validate([
            'name' => 'required|string',
            'description' => 'required|string',
            'price' => 'required|numeric',
            'deposit' => 'required|numeric',
        ]);

        $data_object = new ProductBuilderDataObject(
            $validated_data['name'],
            $validated_data['description'],
            $validated_data['price'],
            $validated_data['deposit']
        );

        return $data_object;
    }
}
