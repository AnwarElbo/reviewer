<?php

namespace App\Http\Controllers;

use Illuminate\View\View;

class MemberController extends Controller
{
    public function index(): View
    {
        return view('spa_auth');
    }
}
