<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class ReviewResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id'         => $this->id,
            'name'       => $this->name,
            'email'      => $this->when(Auth::check(), $this->email),
            'quality'    => $this->quality,
            'service'    => $this->service,
            'price'      => $this->price,
            'comment'    => $this->comment,
            'ip_address' => $this->when(Auth::check(), $this->ip_address),
            'allow'      => $this->when(Auth::check(), $this->allow)
        ];
    }
}
