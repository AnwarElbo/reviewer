<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ShopResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id'              => $this->id,
            'name'            => $this->name,
            'title'           => $this->title,
            'description'     => $this->description,
            'slug'            => $this->slug,
            'phone_number'    => $this->contact->phone_number,
            'viewed'          => $this->viewed,
            'postal_code'     => $this->contact->postal_code,
            'street'          => $this->contact->street,
            'cities'          => CityResource::collection($this->contact->cities),
            'city'            => CityResource::make($this->contact->city),
            'house_number'    => $this->contact->house_number,
            'image'           => $this->images->first(),
            'images'          => $this->images,
            'reviews'         => ReviewResource::collection($this->reviews),
            'average_rating'  => $this->average_rating,
            'opening_hours'   => $this->openingHours,
            'is_open'         => $this->isOpen(),
            'current_day'     => $this->currentShopOpeningHour(),
            'products'        => ProductResource::collection($this->products)
        ];
    }
}
