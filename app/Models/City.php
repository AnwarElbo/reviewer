<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class City extends Model
{
    use SoftDeletes;

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return void
     */
    protected static function boot(): void
    {
        parent::boot();

        static::addGlobalScope(static function (Builder $builder) {
            $builder->orderBy('name');
        });
    }

    /**
     * @return BelongsTo
     */
    public function province(): BelongsTo
    {
        return $this->belongsTo(Province::class, 'province_id');
    }

    /**
     * @return bool
     */
    public function isPrimary(): bool
    {
        return $this->pivot->primary === 1;
    }

    /**
     * @return BelongsToMany
     */
    public function shopContacts(): BelongsToMany
    {
        return $this->belongsToMany(ShopContact::class, 'city_contact_shops');
    }
}