<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ShopOpeningHour extends Model
{
    public $timestamps = false;

    protected $casts = [
        'open_till' => 'datetime:H:i',
        'open_from' => 'datetime:H:i'
    ];
    /**
     * @return BelongsTo
     */
    public function shop(): BelongsTo
    {
        return $this->belongsTo(Shop::class);
    }
}
