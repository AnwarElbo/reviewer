<?php

namespace App\Models;

use App\Spot\Helpers\Spot;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;

class Shop extends Model
{
    use SoftDeletes;

    protected $appends = ['images', 'average_rating'];

    /**
     * @return HasOne
     */
    public function contact(): HasOne
    {
        return $this->hasOne(ShopContact::class);
    }

    /**
     * @return HasMany
     */
    public function openingHours(): HasMany
    {
        return $this->hasMany(ShopOpeningHour::class);
    }

    /**
     * @return HasMany
     */
    public function reviews(): HasMany
    {
        return $this->hasMany(ShopReview::class);
    }

    /**
     * @return BelongsToMany
     */
    public function products(): BelongsToMany
    {
        return $this->belongsToMany(Product::class);
    }

    /**
     * @return Collection
     */
    public function getImagesAttribute(): Collection
    {
        return collect(Storage::disk('public')->files($this->id))->map(static function (string $file) {
            return Storage::disk('public')->url($file);
        });
    }

    /**
     * @return float
     */
    public function getAverageRatingAttribute(): float
    {
        return floor($this->reviews->flatMap(static function (ShopReview $review) {
                    return [$review->quality, $review->service, $review->price];
                })->average() * 2) / 2;
    }

    /**
     * @param int $increase_by
     *
     * @return int
     */
    public function increaseViews(int $increase_by = 1): int
    {
        $this->viewed += $increase_by;
        $this->save();

        return $this->viewed;
    }

    /**
     * @return bool
     */
    public function isOpen(): bool
    {
        $now          = Carbon::now()->format('H:i');
        $opening_hour = $this->currentShopOpeningHour();

        $open_from = $opening_hour->open_from->format('H:i');
        $open_till = $opening_hour->open_till->format('H:i');

        if ($open_from > $open_till) {
            $open_till = $opening_hour->open_till->format('H:i');

            return $now <= $open_till || $now >= $open_from;
        }

        return $now >= $open_from && $now <= $open_till;
    }


    /**
     * @return ShopOpeningHour
     */
    public function currentShopOpeningHour(): ShopOpeningHour
    {
        $now = Carbon::now()->format('H:i');

        $opening_hour = $this->openingHours->first(static function (ShopOpeningHour $opening_hour) use ($now) {
            if (Spot::yesterdayOfWeek() === $opening_hour->day) {
                $open_from = $opening_hour->open_from->format('H:i');
                $open_till = $opening_hour->open_till->format('H:i');

                return $open_from > $open_till && $open_till > $now;
            }

            return false;
        });

        if (! $opening_hour) {
            $opening_hour = $this->openingHours->first(static function (ShopOpeningHour $opening_hour) {
                return Spot::dayOfWeek() === $opening_hour->day;
            });
        }

        return $opening_hour;
    }
}
