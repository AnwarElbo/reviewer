<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class Product extends Model
{
    use SoftDeletes;

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $appends = ['image'];

    /**
     * @return BelongsToMany
     */
    public function shops(): BelongsToMany
    {
        return $this->belongsToMany(Shop::class);
    }

    /**
     * @return string|null
     */
    public function getImageAttribute(): ?string
    {
        $file = collect(Storage::disk('public')->files('products'))->first(function (string $file) {
            $file = last(explode('/', $file));
            return ((int)explode('.', $file, 1)[0]) === $this->id;
        });

        if (! $file) {
            return null;
        }

        return Storage::disk('public')->url($file);
    }
}
