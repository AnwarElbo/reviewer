<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class ShopContact extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $with = ['cities'];

    /**
     * @var array
     */
    protected $appends = ['city'];

    /**
     * @return BelongsTo
     */
    public function shop(): BelongsTo
    {
        return $this->belongsTo(Shop::class);
    }

    /**
     * @return BelongsToMany
     */
    public function cities(): BelongsToMany
    {
        return $this->belongsToMany(City::class, 'city_contact_shops')->withPivot('primary');
    }

    /**
     * @return City
     */
    public function getCityAttribute(): City
    {
        return $this->cities->first(static function (City $city) {
            return $city->isPrimary();
        });
    }
}
