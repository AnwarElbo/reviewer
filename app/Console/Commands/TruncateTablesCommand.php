<?php

namespace App\Console\Commands;

use App\Models\Product;
use App\Models\ShopOpeningHour;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class TruncateTablesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'spot:truncate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Truncates all tables except users and cities';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (config('app.env') === 'production') {
            $this->info('This is a production environment');
            return;
        }

        DB::table('product_shop')->truncate();
        DB::table('shop_opening_hours')->truncate();
        DB::table('city_contact_shops')->truncate();
        DB::table('contacts')->truncate();
        DB::table('products')->truncate();
        DB::table('shop_contacts')->truncate();
        DB::table('shop_reviews')->truncate();
        DB::table('shops')->delete();

        $this->info('Truncated all tables');
    }
}
