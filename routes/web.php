<?php
Route::prefix('api/')->group(static function () {
    Route::get('/products/{product}/edit', 'ProductController@show')->name('product.show');
    Route::get('/shops/{shop}/show', 'ShopController@show')->name('shop.show');
    Route::post('/shops/{shop}/review', 'ReviewController@create')->name('shop.review.create');
    Route::post('/shops/{shop}/view', 'ReviewController@increaseView')->name('shop.increase_view');
    Route::post('/contact', 'ContactController@contact')->name('contact');

    Route::prefix('shops')->name('shops.')->group(static function () {
        Route::get('/popular', 'ShopController@popular')->name('popular');
        Route::get('/shops/city/{city}', 'ShopController@city')->name('city');
        Route::get('/shops/province/{province}', 'ShopController@province')->name('province');
    });

    Route::prefix('cities')->name('cities.')->group(static function () {
        Route::get('/cities', 'CityController@index')->name('index');
    });

    Route::prefix('provinces')->name('provinces.')->group(static function () {
        Route::get('/provinces', 'ProvinceController@index')->name('index');
    });

    Route::prefix('auth')->middleware('auth')->name('auth.')->group(static function () {
        Route::prefix('/reviews')->name('reviews.')->group(static function () {
            Route::get('/index', 'ReviewController@index')->name('index');
            Route::post('{review}/toggle-allow', 'ReviewController@toggleAllow')->name('toggle');
        });

        Route::prefix('/provinces')->name('provinces.')->group(static function () {
            Route::get('/overview', 'ProvinceController@overview')->name('overview');
        });
        Route::prefix('/cities')->name('city.')->group(static function () {
            Route::get('/overview', 'CityController@overview')->name('overview');
            Route::get('/{city}/show', 'CityController@show')->name('show');
            Route::put('/{city}/update', 'CityController@update')->name('update');
            Route::post('/create', 'CityController@create')->name('create');
        });

        Route::prefix('/products')->name('products.')->group(static function () {
            Route::get('/index', 'ProductController@index')->name('index');
            Route::post('/create', 'ProductController@create')->name('create');
            Route::delete('{product}/delete', 'ProductController@delete')->name('delete');
            Route::put('/{product}/update', 'ProductController@update')->name('update');
            Route::post('/{product}/image', 'ProductController@uploadImage')->name('image');
        });

        Route::prefix('/shops')->name('shops.')->group(static function () {
            Route::get('/index', 'ShopController@index')->name('index');
            Route::post('/create', 'ShopController@create')->name('create');
            Route::delete('/{shop}/delete', 'ShopController@delete')->name('delete');
            Route::put('/{shop}/update', 'ShopController@update')->name('update');
            Route::post('/{shop}/upload', 'ShopController@uploadImage')->name('image');
        });
    });
});

Route::get('auth/{any?}', 'MemberController@index')->name('auth')->where('any', '([A-z\d-\/_.]+)?')->middleware('auth');

Route::fallback('SpaController@index')->name('spa');

Auth::routes();