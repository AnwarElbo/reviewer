<?php

/* @var $factory Factory */

use App\Model;
use App\Models\City;
use App\Models\Shop;
use App\Models\ShopContact;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\Str;

$factory->define(ShopContact::class, static function (Faker $faker) {
    return [
        'shop_id' => static function () {
            return factory(Shop::class)->create()->id;
        },
        'phone_number' => $faker->phoneNumber,
        'street' => $faker->streetName,
        'house_number' => $faker->numberBetween(0, 100),
        'postal_code' => $faker->numberBetween(1000, 9999) . ' ' . strtoupper(Str::random(2)),
    ];
});
