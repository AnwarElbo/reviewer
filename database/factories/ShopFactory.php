<?php

/* @var $factory Factory */

use App\Model;
use App\Models\Shop;
use Faker\Generator as Faker;
use Faker\Factory as FakerFactory;
use Illuminate\Database\Eloquent\Factory;

$faker_us = FakerFactory::create('en_US');

$factory->define(Shop::class, function (Faker $faker) use ($faker_us) {
    return [
        'name' => $faker->company,
        'title' => $faker_us->catchPhrase,
        'description' => $faker->text(),
        'slug' => $faker->unique()->slug(3),
        'viewed' => 0
    ];
});
