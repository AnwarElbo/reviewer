<?php

/* @var $factory Factory */

use App\Model;
use App\Models\Shop;
use App\Models\ShopOpeningHour;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(ShopOpeningHour::class, function (Faker $faker) {
    return [
        'shop_id'   => static function () {
            return factory(Shop::class)->create()->id;
        },
        'day'       => $faker->numberBetween(0, 6),
        'open_from' => $faker->dateTime,
        'open_till' => $faker->dateTime
    ];
});
