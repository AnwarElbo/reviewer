<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shops', static function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('title');
            $table->text('description');
            $table->string('slug');
            $table->unsignedInteger('viewed');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('shop_reviews', static function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('shop_id');
            $table->string('name');
            $table->string('email');
            $table->decimal('quality');
            $table->decimal('service');
            $table->decimal('price');
            $table->text('comment');
            $table->ipAddress('ip_address');
            $table->unsignedTinyInteger('allow')->default(0);

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('shop_id')->references('id')->on('shops');
        });

        Schema::create('shop_contacts', static function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('shop_id');
            $table->string('phone_number');
            $table->string('street');
            $table->string('house_number');
            $table->string('city');
            $table->string('postal_code');

            $table->foreign('shop_id')->references('id')->on('shops');
        });

        Schema::create('shop_opening_hours', static function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('shop_id');
            $table->unsignedTinyInteger('day');
            $table->timestamp('open_from')->nullable();
            $table->timestamp('open_till')->nullable();

            $table->foreign('shop_id')->references('id')->on('shops');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_contacts');
        Schema::dropIfExists('shop_opening_hours');
        Schema::dropIfExists('shop_reviews');
        Schema::dropIfExists('shops');
    }
}
