<?php

use App\Models\ShopContact;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCityShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('city_contact_shops', static function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('shop_contact_id');
            $table->unsignedInteger('city_id');

            $table->foreign('shop_contact_id')->references('id')->on('shop_contacts');
            $table->foreign('city_id')->references('id')->on('cities');
            $table->boolean('primary')->default(0);
        });

        ShopContact::all()->each(static function (ShopContact $shop_contact) {
            $shop_contact->cities()->sync([$shop_contact->city_id => ['primary' => true]]);
        });

        Schema::table('shop_contacts', static function (Blueprint $table) {
            $table->dropForeign('shop_contacts_city_id_foreign');
            $table->dropColumn('city_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('city_shops');
    }
}
