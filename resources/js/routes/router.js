import VueRouter from "vue-router";
import Vue from "vue";
import Home from "../views/Home";
import Login from "../views/Login";
import ShopOverview from "../views/Auth/ShopOverview";
import CreateShop from "../views/Auth/CreateShop";
import ShopImages from "../views/Auth/ShopImages";
import EditShop from "../views/Auth/EditShop";
import ShopDetail from "../views/ShopDetail";
import CityShopsOverview from "../views/CityShopsOverview";
import ProductsOverview from "../views/Auth/ProductsOverview";
import CreateProduct from "../views/Auth/CreateProduct";
import EditProduct from "../views/Auth/EditProduct";
import ProductImage from "../views/Auth/ProductImage";
import ReviewsOverview from "../views/Auth/ReviewsOverview";
import NotFound from "../views/NotFound";
import Contact from "../views/Contact";
import ProvinceShopsOverview from "../views/ProvinceShopsOverview";
import CityOverview from "../views/Auth/CityOverview";
import EditCity from "../views/Auth/EditCity";
import CreateCity from "../views/Auth/CreateCity";

Vue.use(VueRouter);

export default new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home,
            title: 'Populaire lachgas verkopers'
        }, {
            path: '/lachgas/:city/:slug/:shop',
            name: 'shop.detail',
            component: ShopDetail
        }, {
            path: '/lachgas/:city',
            name: 'shops.city.overview',
            component: CityShopsOverview
        }, {
            path: '/lachgas/provincie/:province',
            name: 'shops.province.overview',
            component: ProvinceShopsOverview
        }, {
            path: '/login',
            name: 'sign_in',
            component: Login
        }, {
            path: '/contact',
            name: 'contact',
            component: Contact
        }, {
            path: '/auth/shops',
            name: 'auth.shops',
            component: ShopOverview
        }, {
            path: '/auth/shops/create',
            name: 'auth.shops.create',
            component: CreateShop
        }, {
            path: '/auth/shops/edit/:shop',
            name: 'auth.shops.edit',
            component: EditShop
        }, {
            path: '/auth/shops/images/:shop',
            name: 'auth.shops.images',
            component: ShopImages
        }, {
            path: '/auth/products/images/:product',
            name: 'auth.products.image',
            component: ProductImage
        }, {
            path: '/auth/cities',
            name: 'auth.cities',
            component: CityOverview
        }, {
            path: '/auth/cities/:city/edit',
            name: 'auth.cities.edit',
            component: EditCity
        }, {
            path: '/auth/cities/create',
            name: 'auth.cities.create',
            component: CreateCity
        }, {
            path: '/auth/products',
            name: 'auth.products',
            component: ProductsOverview
        }, {
            path: '/auth/products/create',
            name: 'auth.products.create',
            component: CreateProduct
        }, {
            path: '/auth/products/edit/:product',
            name: 'auth.products.edit',
            component: EditProduct
        }, {
            path: '/auth/reviews',
            name: 'auth.reviews',
            component: ReviewsOverview
        }, {
            path: '*',
            name: '404',
            component: NotFound
        }
    ],
    scrollBehavior() {
        return window.scrollTo({top: 0, behavior: 'smooth'});
    }
});