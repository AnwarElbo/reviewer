import {Vue, router} from './base-vue';
import store from './store/frontend';
import MainMenu from "./components/PageComponents/MainMenu";
import ShopCard from './components/ShopComponents/ShopCard.vue';
import ShopBanner from './components/ShopComponents/ShopBanner.vue';
import ShopDescription from './components/ShopComponents/ShopDescription.vue';
import ShopOpeningHours from './components/ShopComponents/ShopOpeningHours.vue';
import ShopContactInfo from './components/ShopComponents/ShopContactInfo.vue';
import ShopReviewForm from './components/ShopComponents/ShopReviewForm.vue';
import CityOverview from "./components/CityComponents/CityOverview";
import SearchForm from "./components/SearchComponents/SearchForm";
import ShopOverviewTitle from "./components/ShopComponents/ShopOverviewTitle";

Vue.component('search-form', SearchForm);
Vue.component('shop-overview-title', ShopOverviewTitle);
Vue.component('shop-card', ShopCard);
Vue.component('city-overview', CityOverview);
Vue.component('main-menu', MainMenu);
Vue.component('shop-opening-hours', ShopOpeningHours);
Vue.component('shop-contact-info', ShopContactInfo);
Vue.component('shop-banner', ShopBanner);
Vue.component('shop-review-form', ShopReviewForm);
Vue.component('shop-description', ShopDescription);

const app = new Vue({
    el: '#app',
    store,
    router
});
