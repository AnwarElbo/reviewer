export default {
    namespaced: true,
    state: {
        cities: [],
        selected_city: {
            name: '',
            id: 0,
            province_id: null
        }
    },
    actions: {
        fetchCities({commit}) {
            axios.get(route('cities.index')).then(response => {
                commit('setCities', response.data);
            });
        },
        fetchAllCities({commit}) {
            axios.get(route('auth.city.overview')).then(response => {
                commit('setCities', response.data);
            });
        },
        resetSelectedCity({commit}) {
            commit('setSelectedCity', {
                name: '',
                id: 0,
                province_id: null
            })
        },
        setSelectedCity({commit}, city) {
            commit('setSelectedCity', city);
        },
        fetchCity({commit}, city) {
            axios.get(route('auth.city.show', {city: city})).then(response => {
                commit('setSelectedCity', response.data);
            })
        }
    },
    mutations: {
        setCities(state, cities) {
            state.cities = cities;
        },
        setSelectedCity(state, selected_city) {
            state.selected_city = selected_city;
        }
    }
}