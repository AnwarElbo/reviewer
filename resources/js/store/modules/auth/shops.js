import axios from 'axios';

export default {
    namespaced: true,
    state: {
        shops: [],
        selected_shop: {
            name: '',
            title: '',
            description: '',
            slug: '',
            phone_number: '',
            street: '',
            house_number: '',
            cities: [],
            city: [],
            postal_code: ''
        },
    },
    actions: {
        fetchShops({commit}) {
            axios.get(route('auth.shops.index')).then(response => {
                commit('setShops', response.data);
            });
        },
        fetchPopularShops({commit}) {
            axios.get(route('shops.popular')).then(response => {
                commit('setShops', response.data);
            });
        },
        fetchShopsByCity({commit}, city) {
            axios.get(route('shops.city', {city: city})).then(response => {
                commit('setShops', response.data);
            });
        },
        fetchShopsByProvince({commit}, province) {
            axios.get(route('shops.province', {province: province})).then(response => {
                commit('setShops', response.data);
            });
        },
        fetchShop({commit}, shop_id) {
            axios.get(route('shop.show', {shop: shop_id})).then(response => {
                commit('setSelectedShop', response.data);
            });
        },
        resetSelectedShop({commit}) {
            commit('setSelectedShop',  {
                name: '',
                title: '',
                description: '',
                slug: '',
                phone_number: '',
                street: '',
                house_number: '',
                cities: [],
                city: [],
                postal_code: ''
            });
        }
    },
    mutations: {
        setShops(state, shops) {
            state.shops = shops;
        },
        setSelectedShop(state, shop) {
            state.selected_shop = shop;
        }
    }
}