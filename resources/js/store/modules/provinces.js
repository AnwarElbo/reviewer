export default {
    namespaced: true,
    state: {
        provinces: []
    },
    actions: {
        fetchProvinces({commit}) {
            axios.get(route('provinces.index')).then(response => {
                commit('setProvinces', response.data);
            })
        },
        fetchAllProvinces({commit}) {
            axios.get(route('auth.provinces.overview')).then(response => {
                commit('setProvinces', response.data);
            })
        }
    },
    mutations: {
        setProvinces(state, provinces) {
            state.provinces = provinces;
        }
    }
}