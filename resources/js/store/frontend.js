import Vuex from 'vuex';
import Vue from 'vue';
import menu from './modules/menu';
import shops from './modules/auth/shops';
import cities from './modules/cities';
import provinces from "./modules/provinces";

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        menu,
        shops,
        cities,
        provinces
    }
});